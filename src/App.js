import React from 'react';
import { Provider as ReduxProvider } from 'react-redux';

//store
import store from 'appRedux/store';

//components
import Viewport from 'components/Viewport';
import Routes from 'Routes';

const App = () => {

  return (
    <ReduxProvider store={store}>
      <Viewport>
        <Routes />
      </Viewport>
    </ReduxProvider>
  );
}

export default App;
