export default {
  HOME_PAGE: '/',
  ANIME_PAGE_ROUTE: '/view/:animeId',
  ANIME_PAGE: '/view',
};