import configureMockStore from "redux-mock-store";
import thunk from "redux-thunk";
import fetchMock from "fetch-mock";
import expect from "expect";

//actions
import * as animeActions from "appRedux/actions/anime";

//constants
import actTypes from "appRedux/actionTypes";
import { baseUrl } from "constants/config";


describe("anime async actions", () => {
  const middlewares = [thunk];
  const mockStore = configureMockStore(middlewares);

  afterEach(() => {
    fetchMock.restore();
  });

  it("simulate success call", () => {
    const searchKey = "sampleSearchKey";
    const mockTestResult = [
      {
        title: "test result"
      }
    ];
    fetchMock.getOnce(`${baseUrl}/search/anime?q=${searchKey}`, {
      body: {
        results: mockTestResult
      },
      headers: { "content-type": "application/json" }
    });

    const expectedActions = [
      { type: actTypes.SEARCH_ANIME },
      {
        type: actTypes.SEARCH_ANIME_SUCCESS,
        payload: {
          animeList: mockTestResult
        }
      }
    ];
    const store = mockStore({ activeAnime: [] });

    return store.dispatch(animeActions.searchAnimeList(searchKey)).then(() => {
      expect(store.getActions()).toEqual(expectedActions);
    });
  });

  it("simulate failed call", () => {
    const searchKey = "sampleSearchKey";
    fetchMock.getOnce(`${baseUrl}/search/anime?q=${searchKey}`, {
      throws: "API call failed"
    });

    const expectedActions = [
      { type: actTypes.SEARCH_ANIME },
      { type: actTypes.SEARCH_ANIME_FAILED },
    ];
    const store = mockStore({ activeAnime: [] });

    return store.dispatch(animeActions.searchAnimeList(searchKey)).then(() => {
      expect(store.getActions()).toEqual(expectedActions);
    });
  });
});
