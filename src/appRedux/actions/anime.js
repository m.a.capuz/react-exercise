import { request } from "utils/helpers";

//configs
import actTypes from "appRedux/actionTypes";
import { baseUrl } from "constants/config";

const searchAnime = () => ({
  type: actTypes.SEARCH_ANIME,
});

const searchAnimeSuccess = payload => ({
  type: actTypes.SEARCH_ANIME_SUCCESS,
  payload
});

const searchAnimeFailed = payload => ({
  type: actTypes.SEARCH_ANIME_FAILED,
});

export const searchAnimeList = searchKey => {

  return async dispatch => {
    dispatch(searchAnime());
    try {
      const resp = await request.get(`${baseUrl}/search/anime?q=${searchKey}`);
      if (resp) {
        dispatch(searchAnimeSuccess({
          animeList: resp.results
        }));
      }
    } catch(err) {
      dispatch(searchAnimeFailed());
    }
  }
}

const getDetails = () => ({
  type: actTypes.GET_ANIME_DETAILS,
});

const getDetailsSuccess = payload => ({
  type: actTypes.GET_ANIME_DETAILS_SUCCESS,
  payload
});

const getDetailsFailed = payload => ({
  type: actTypes.GET_ANIME_DETAILS_FAILED,
});

export const getAnimeDetails = animeId => {

  return async dispatch => {
    dispatch(getDetails());
    try {
      const resp = await request.get(`${baseUrl}/anime/${animeId}`);
      if(resp){
        dispatch(getDetailsSuccess({
          animeDetails: resp
        }));
      }
    } catch(err) {
      dispatch(getDetailsFailed());
    }
  }
}

