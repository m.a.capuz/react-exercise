import { combineReducers } from "redux";
import anime from "./anime.js";

export default combineReducers({
  anime
});
