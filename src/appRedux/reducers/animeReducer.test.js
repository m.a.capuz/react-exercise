import animeReducer from "appRedux/reducers/anime";

//constants
import actTypes from "appRedux/actionTypes";

const initialState = {
  isFetching: false,
  searchList: [],
  activeAnime: {}
};

describe("todos reducer", () => {
  it("should return the initial state", () => {
    expect(animeReducer(undefined, {})).toEqual(initialState);
  });

  it("handle SEARCH_ANIME", () => {
    expect(
      animeReducer([], {
        type: actTypes.SEARCH_ANIME
      })
    ).toEqual({
      isFetching: true,
    });
  });

  it("handle SEARCH_ANIME_SUCCESS", () => {
    const payload = {
      animeList: ["sampleAnimeList"]
    };

    expect(
      animeReducer([], {
        type: actTypes.SEARCH_ANIME_SUCCESS,
        payload,
      })
    ).toEqual({
      isFetching: false,
      searchList: payload.animeList,
    });
  });

  it("handle SEARCH_ANIME_FAILED", () => {
    expect(
      animeReducer([], {
        type: actTypes.SEARCH_ANIME_FAILED
      })
    ).toEqual({
      isFetching: false,
      searchList: [],
    });
  });
});