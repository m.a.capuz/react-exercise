import actTypes from "appRedux/actionTypes";

const initialState = {
  isFetching: false,
  searchList: [],
  activeAnime: {},
};

export default (state = initialState, action) => {
  const {type, payload} = action || {};
  const { animeList, animeDetails } = payload || {};

  switch (type) {
    case actTypes.SEARCH_ANIME:
      return {
        ...state,
        isFetching: true,
      };
    case actTypes.SEARCH_ANIME_SUCCESS:
      return {
        ...state,
        isFetching: false,
        searchList: animeList,
      };
    case actTypes.SEARCH_ANIME_FAILED:
      return {
        ...state,
        isFetching: false,
        searchList: [],
      };
    case actTypes.GET_ANIME_DETAILS:
      return {
        ...state,
        isFetching: true,
      }
    case actTypes.GET_ANIME_DETAILS_SUCCESS:
      return {
        ...state,
        isFetching: false,
        activeAnime: animeDetails,
      }
    case actTypes.GET_ANIME_DETAILS_FAILED:
      return {
        ...state,
        isFetching: false,
        activeAnime: {},
      }
    default:
      return state;
  }
}