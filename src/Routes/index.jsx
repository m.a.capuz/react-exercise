import React, { Suspense, lazy } from "react";
import {
  BrowserRouter as Router,
  Route,
  Switch,
  Redirect,
} from "react-router-dom";

//components
import Loading from "components/Loading";

//constants
import routes from "constants/routes";

//lazy-loading components
const Home = lazy(() => import('domain/Home'));
const AnimeDetails = lazy(() => import('domain/AnimeDetails'));

const Routes = () => {

  return (
    <Router>
      <Suspense fallback={<Loading />}>
        <Switch>
          <Route exact
            path={routes.HOME_PAGE}
            render={() => {
              return <Home />;
            }}
          />
          <Route exact
            path={routes.ANIME_PAGE_ROUTE}
            render={props => {
              return <AnimeDetails {...props} />;
            }}
          />
          <Route path="*" render={() => <Redirect to={routes.HOME_PAGE} />} />
        </Switch>
      </Suspense>
    </Router>
  );
}

export default Routes;