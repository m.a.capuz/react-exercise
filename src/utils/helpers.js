const handleResponse = (resp) => {   // if empty return {}
  if (resp.ok) {
    return resp.text().then(function (respText) {
      try {
        return JSON.parse(respText);
      } catch (e) {
        return respText;
      }
    });
  }
  throw Error(resp.statusMessage);
}

export const request = {
  get: async (url, params) => {
    return await fetch(url, { method: 'GET', ...params }).then(handleResponse);
  },
}