/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import PropTypes from "prop-types";

//components
import Loading from "components/Loading";
import HeaderDetails from "./HeaderDetails";

//styles
import classes from "./styles.module.scss";

//actions
import { getAnimeDetails } from "appRedux/actions/anime";

const AnimeDetails = ({ history, match }) => {
  const dispatch = useDispatch();
  const isFetching = useSelector(({ anime }) => anime.isFetching);
  const animeDetails = useSelector(({ anime }) => anime.activeAnime);
  const { params } = match;
  const { animeId } = params || {};
  const { mal_id, trailer_url } = animeDetails || {};

  const onClickBack = () => {
    console.log('history', history);
    history.goBack();
  }

  useEffect(() => {
    if(animeId){
      dispatch(getAnimeDetails(animeId));
    }
  }, []);

  return(
    <div className={classes.contentWrapper}>
      {
        isFetching ? <Loading /> :
        <>
          <div className={`${classes.title} ${classes.backBtn}`}
            onClick={onClickBack}
          >
            {`< Back`}
          </div>
          <HeaderDetails />
          <div className={classes.synopsis}>
            <div className={classes.title}>
              Watch Trailer
            </div>
              <iframe className={classes.videoTrailer}
                src={trailer_url || ""}
                title={`trailer${mal_id}`}
              />
          </div>
        </>
      }
    </div>
  );
}

AnimeDetails.propTypes = {
  match: PropTypes.object.isRequired,
};

AnimeDetails.defaultProps = {
  match: {},
};


export default AnimeDetails;