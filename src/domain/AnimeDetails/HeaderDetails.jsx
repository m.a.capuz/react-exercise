import React from "react";
import { useSelector } from "react-redux";

//styles
import classes from "./styles.module.scss";

const SubDetails = ({ title, children }) => {
  return(
    <p>
      <span className={classes.subTitle}>{title}</span>
      {children}
    </p>
  );
}

const HeaderDetails = () => {
  const animeDetails = useSelector(({ anime }) => anime.activeAnime);
  const { image_url, title, type, duration, score, episodes, synopsis } = animeDetails || {};

  return(
    <div className={classes.headerDetails}>
      <img src={image_url} alt={"loading"} />
      <div className={classes.details}>
        <span className={classes.title}>
          {title}
          {type && `(${type})`}
        </span>
        <SubDetails title="Number of Episodes:">
          {episodes}
        </SubDetails>
        <SubDetails title="Duration:">
          {duration}
        </SubDetails>
        <SubDetails title="Ratings:">
          {score}
        </SubDetails>
        <p>{synopsis}</p>
      </div>
    </div>
  );
}

export default HeaderDetails;