import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link } from "react-router-dom";

//components
import Search from "components/Search";
import Loading from "components/Loading";
import AnimeList from "components/AnimeList";

//actions
import { searchAnimeList } from "appRedux/actions/anime";

//hooks
import { useFormInput } from "hooks";

//styles
import classes from "./styles.module.scss";

//constants
import routes from "constants/routes";

const Home = () => {
  const searchVal = useFormInput("");
  const dispatch = useDispatch();
  const isFetching = useSelector(({ anime }) => anime.isFetching);
  const searchList = useSelector(({ anime }) => anime.searchList);

  const onClickSearch = () => {
    dispatch(searchAnimeList(searchVal.value));
  }

  return (
    <div className={classes.contentWrapper}>
      <div>
        <Search
          onChange={searchVal.handleInputChange}
          onSearch={onClickSearch}
        />
      </div>
      <div>
        {isFetching ? <Loading /> :
        <AnimeList>
          {searchList.map(details => (
            <Link to={`${routes.ANIME_PAGE}/${details.mal_id}`} >
              <AnimeList.ListItem {...details}/>
            </Link>
          ))}
        </AnimeList> }
      </div>
    </div>
  );
}

Home.propTypes = {
};

Home.defaultProps = {
};

export default Home;