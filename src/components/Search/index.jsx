import React from "react";
import PropTypes from "prop-types";

//styles
import classes from "./styles.module.scss";

const Search = ({ onSearch, ...props }) => {
  return(
    <div className={classes.inputWrapper}>
      <input type="text"
        name="search"
        placeholder="What do you want to find?"
        {...props}
      />
      <button onClick={onSearch}>
        Search
      </button>
    </div>
  );
}

Search.propTypes = {
  onSearch: PropTypes.func.isRequired,
};

Search.defaultProps = {
  onSearch: () => {},
};

export default Search;