import React from "react";
import { shallow, mount } from "enzyme";
import Search from "components/Search";

describe("Search: input anime", () => {
  let wrapper;
  const handleSearch = jest.fn();

  beforeEach(() => {
    wrapper = mount(<Search onSearch={handleSearch} />);
  });

  afterEach(() => {
    wrapper.unmount();
  });

  it("Matches snapshot", () => {
    expect(wrapper).toMatchSnapshot();
  });
});
