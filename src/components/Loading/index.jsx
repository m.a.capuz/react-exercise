import React from "react";

//styles
import classes from "./styles.module.scss";

const Loading = () => {
  return (
    <div className={classes.loader}></div>
  );
}

export default Loading;