import React from "react";
import { shallow, mount } from "enzyme";
import Loading from "components/Loading";

describe("Loading Screen", () => {
  let wrapper;

  beforeEach(() => {
    wrapper = mount(<Loading />);
  });

  afterEach(() => {
    wrapper.unmount();
  });

  it("Matches snapshot", () => {
    expect(wrapper).toMatchSnapshot();
  });
});
