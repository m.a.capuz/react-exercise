import React from "react";
import PropTypes from "prop-types";

//components
import ListItem from "./ListItem";

//styles
import classes from "./styles.module.scss";

const AnimeList = ({ children }) => {
  return(
    <div className={classes.listWrapper}>
      {children}
    </div>
  );
}

AnimeList.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node
  ]).isRequired
};

AnimeList.defaultProps = {
  children: ""
};


AnimeList.ListItem = ListItem;

export default AnimeList;