import React from "react";
import PropTypes from "prop-types";

//components
import Loading from "components/Loading";

//styles
import classes from "./styles.module.scss";

const ListItem = ({ image_url, title }) => {

  return (
    <div className={classes.listItem}>
      <img src={image_url} alt={<Loading />} />
      <div className={classes.title}>{title}</div>
    </div>
  );
}

ListItem.propTypes = {
  title: PropTypes.string.isRequired,
  image_url: PropTypes.string,
};

ListItem.defaultProps = {
  image_url: "",
  title: "",
};

export default ListItem;