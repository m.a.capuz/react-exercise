import React from "react";
import { mount } from "enzyme";
import ListItem from "components/AnimeList/ListItem";

describe("Anime list", () => {
  let wrapper;
  let title = "sampleTitle";
  let image_url = "sampleImageUrl";

  beforeEach(() => {
    wrapper = mount(<ListItem />);
  });

  afterEach(() => {
    wrapper.unmount();
  });

  it("Matches snapshot", () => {
    expect(wrapper).toMatchSnapshot();
  });

  it("renders props", () => {
    wrapper = mount(
      <ListItem
        image_url={image_url}
        title={title}
      />
    );
    const image = wrapper.find("img");
    const titleElement = wrapper.find(".title");
    expect(image.prop("src")).toEqual(image_url);
    expect(titleElement.prop("children")).toEqual(title);
  });

});
