import React from "react";
import { shallow, mount } from "enzyme";
import AnimeList from "components/AnimeList";

describe("Anime list", () => {
  let wrapper;

  beforeEach(() => {
    wrapper = mount(<AnimeList />);
  });

  afterEach(() => {
    wrapper.unmount();
  });

  it("Matches snapshot", () => {
    expect(wrapper).toMatchSnapshot();
  });
});
