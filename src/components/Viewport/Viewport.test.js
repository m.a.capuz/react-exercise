import React from "react";
import { shallow, mount } from "enzyme";
import Viewport from "components/Viewport";

describe("Viewport: wholescreen view", () => {
  let wrapper;

  beforeEach(() => {
    wrapper = mount(<Viewport />);
  });

  afterEach(() => {
    wrapper.unmount();
  });

  it("Matches snapshot", () => {
    expect(wrapper).toMatchSnapshot();
  });
});
