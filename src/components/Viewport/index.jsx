import React from "react";
import PropTypes from "prop-types";

//styles
import classes from "./styles.module.scss";

const Viewport = ({ children }) => {
  return (
    <div className={classes.viewportWrapper}>
      {children}
    </div>
  );
}

Viewport.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node
  ]).isRequired
};

Viewport.defaultProps = {
  children: ""
};

export default Viewport;